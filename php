FROM php:7-fpm
MAINTAINER Daniel Owens <kyoji1@gmail.com>

RUN apt-get update && apt-get install -y\
    libpq-dev \
    libmemcached-dev \
    curl

#Xdebug
RUN pecl install xdebug \
    && docker-php-ext-enable xdebug
COPY config/xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-debug.ini
COPY config/php.ini /usr/local/etc/php/

#PDO mysql Driver
RUN docker-php-ext-install pdo pdo_mysql \
    && docker-php-ext-enable pdo_mysql

#CMD ["php-fpm"]