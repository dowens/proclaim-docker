-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE DATABASE `proclaim` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `proclaim`;

DROP TABLE IF EXISTS `proclaim_posts`;
CREATE TABLE `proclaim_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `proclaim_posts` (`ID`, `post_author`, `post_date`, `post_name`, `post_content`, `post_slug`) VALUES
  (1,	1,	'2016-12-10 10:32:31',	'Test Post 1',	'This is some testing text. Do not read into this.',	'test-post-one');

DROP TABLE IF EXISTS `proclaim_taxonomies`;
CREATE TABLE `proclaim_taxonomies` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `taxonomy_slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `taxonomy_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `taxonomy_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `proclaim_taxonomies` (`ID`, `taxonomy_slug`, `taxonomy_name`, `taxonomy_type`) VALUES
  (1,	'test-tax',	'Test Taxonomy',	'Post');

DROP TABLE IF EXISTS `proclaim_users`;
CREATE TABLE `proclaim_users` (
  `ID` int(5) unsigned NOT NULL,
  `user_password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_role` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `proclaim_users` (`ID`, `user_password`, `user_name`, `user_role`) VALUES
  (263,	'$2y$10$HOWOkML8jRpuAJjeGufSg.t1gSHPvfwvhqxe7ClgDejWlLnHbNUi6',	'admin',	'admin');

-- 2018-03-20 01:38:19